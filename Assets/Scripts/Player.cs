﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    #region properties
    [SerializeField] float _speed = 3.5f;
    [SerializeField] private GameObject _laserPrefab;
    [SerializeField] private GameObject _laserTriplePrefab;
    [SerializeField] private GameObject _shield;
    [SerializeField] private GameObject[] _hurts;
    [SerializeField] private GameObject _mainCamera;
    [SerializeField] private AudioClip _laserClip;
    [SerializeField] private int _lives = 3;
    [SerializeField] private int _ammo = 16; //it is set to 16 cause it consider the initial shot against the asteroid so it may start the battle with 15
    [SerializeField] private float _shootRate = 0.5f;
    [SerializeField] private bool _isTripleShotEnabled = false;
    [SerializeField] private bool _isSpeedBoostEnabled = false;
    [SerializeField] private bool _isShieldEnabled = false;
    [SerializeField] private float _coolDownRate = 3f;
    [SerializeField] private int _score = 0;
    [SerializeField] private bool _isSeekingTarget = false;    
    private AudioSource _audioSource;
    private float _canFire = -1f;
    private float _speedIncrease = 2f;
    private int _hurtIndex = -1;
    private int _shieldStrength = 3;
    private int _thrustlerLimit = 15;
    private float _currentThrustler = 0;
    private SpriteRenderer _shieldSpriteRenderer;
    private CameraShake _cameraShake;
    private WaitForSeconds waitForSecs;
    private WaitForSeconds _seekingTimeWaitForSecs;
    private WaitForSeconds _restartThrustlerWaitForSecs;
    #endregion

    #region UnityMethods
    void Start()
    {
        transform.position = new Vector3(0, 0, 0);
        waitForSecs = new WaitForSeconds(_coolDownRate);
        _audioSource = gameObject.GetComponent<AudioSource>();
        _audioSource.clip = _laserClip;
        _shieldSpriteRenderer = _shield.GetComponent<SpriteRenderer>();
        _cameraShake = _mainCamera.GetComponent<CameraShake>();
        _seekingTimeWaitForSecs = new WaitForSeconds(5f);
        _restartThrustlerWaitForSecs = new WaitForSeconds(3f);
    }


    void Update()
    {
        

        _currentThrustler += Time.deltaTime;

        if (_currentThrustler <= _thrustlerLimit)
        {
            UIManager.Instance.UpdateThrustler(_currentThrustler);
            Move();
            BoundariesCalculation();
        }
        else
            StartCoroutine("restartThrustlerRoutine");

        if (Input.GetKeyDown(KeyCode.Space) && Time.time > _canFire && _ammo > 0)
        {
            FireLaser();
        }       
    }
    #endregion

    #region PrivateMethods
    private void Move()
    {
        Vector3 translatePosition = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        float currentSpeed = _speed;

        if (Input.GetKey(KeyCode.LeftShift))
            currentSpeed = _speed * _speedIncrease;

        if (_isSpeedBoostEnabled)
        {
            transform.Translate(translatePosition * (currentSpeed * _speedIncrease) * Time.deltaTime);
        }
        else
        {
            transform.Translate(translatePosition * currentSpeed * Time.deltaTime);
        }

    }

    private void BoundariesCalculation()
    {
        Vector3 screenLimit = transform.position;

        if (transform.position.y >= 0)
        {
            screenLimit.y = 0;
            transform.position = screenLimit;
        }
        else if (transform.position.y <= -3.8f)
        {
            screenLimit.y = -3.8f;
            transform.position = screenLimit;
        }

        if (transform.position.x >= 10f)
        {
            screenLimit.x = -10f;
            transform.position = screenLimit;
        }
        else if (transform.position.x <= -10f)
        {
            screenLimit.x = 10f;
            transform.position = screenLimit;
        }
    }

    private void FireLaser()
    {
        _canFire = Time.time + _shootRate;
        if (_isTripleShotEnabled)
        {
            var laser = Instantiate(_laserTriplePrefab, transform.position, Quaternion.identity);            
        }
        else
        {
            var laser = Instantiate(_laserPrefab, new Vector3(transform.position.x, transform.position.y + 1.05f, 0), Quaternion.identity);
            laser.gameObject.GetComponent<Laser>().SetSeekingTarget(_isSeekingTarget);
        }

        _ammo--;
        UIManager.Instance.UpdateAmmo(_ammo);
        _audioSource.Play();
    }
    private void SetColorShield()
    {
        switch (_shieldStrength)
        {
            case 1:
                _shieldSpriteRenderer.color = Color.red;
                break;
            case 2:
                _shieldSpriteRenderer.color = Color.green;
                break;
        }
    }

    private void RandomHurtWing()
    {
        int initialHurt = Random.Range(0, 2);
        _hurtIndex = _hurtIndex == -1 ? initialHurt : _hurtIndex == 1 ? 0 : 1;
        _hurts[_hurtIndex].SetActive(true);
    }


    #endregion

    #region Coroutines
    IEnumerator DeactivateTripleShotRountine()
    {
        yield return waitForSecs;
        _isTripleShotEnabled = false;
    }

    IEnumerator DeactivateSpeedRountine()
    {
        yield return waitForSecs;
        _isSpeedBoostEnabled = false;
    }
    IEnumerator SetSeekingTimeRoutine()
    {
        yield return _seekingTimeWaitForSecs;
        _isSeekingTarget = false;
    }
    IEnumerator restartThrustlerRoutine()
    {        
        yield return _restartThrustlerWaitForSecs;
        _currentThrustler = 0;
    }
    #endregion

    #region PublicMethods
    public void Damage()
    {
        if (_isShieldEnabled)
        {
            _shieldStrength--;
            SetColorShield();

            if (_shieldStrength == 0)
            {
                _isShieldEnabled = false;
                _shield.SetActive(false);
                _shieldStrength = 3;
                _shieldSpriteRenderer.color = Color.white;
            }
            return;
        }

        StartCoroutine(_cameraShake.Shake(0.15f, 0.1f));
        

        _lives--;
        RandomHurtWing();
        UIManager.Instance.UpdateLives(_lives);

        if (_lives == 0)
        {
            SpawnManager.Instance.StopSpawning();
            Destroy(this.gameObject);
        }
    }

    public void ActivateTripleShot()
    {
        _isTripleShotEnabled = true;
        StartCoroutine("DeactivateTripleShotRountine");
    }



    public void ActivateSpeed()
    {
        _isSpeedBoostEnabled = true;
        StartCoroutine("DeactivateSpeedRountine");
    }

 

    public void ActivateShield()
    {
        _shieldStrength = 3;
        _shieldSpriteRenderer.color = Color.white;
        _isShieldEnabled = true;
        _shield.SetActive(true);
    }

    public void ScorePoints(int points)
    {
        _score += points;
        UIManager.Instance.UpdateScore(_score);
    }

    public void RechargeAmmo()
    {
        _ammo = 15;
        UIManager.Instance.UpdateAmmo(_ammo);
    }

    public void RechargeHealth()
    {
        if (_lives < 3)
        {
            _lives++;
            UIManager.Instance.UpdateLives(_lives);
            _hurtIndex = _hurts[0].activeSelf ? 0 :1;
            _hurts[_hurtIndex].SetActive(false);
        }
    }

    public void SetSeekingTarget()
    {
        _isSeekingTarget = true;
        StartCoroutine("SetSeekingTimeRoutine");
    }
    #endregion

}
