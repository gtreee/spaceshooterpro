﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    #region Properties
    [SerializeField] private float _speed = 4f;
    [SerializeField] private GameObject _laserPrefab;
    [SerializeField] private float _shootRate = 3f;    
    private float _canFire = -1f;
    private AudioSource _audioSource;
    private Player _player;
    private Animator _animator;
    private Collider2D _collider2d;
    private bool _isDeath=false;
    #endregion

    #region UnityMethods

    void Start()
    {
        _player = GameObject.Find("Player").GetComponent<Player>();
        _animator = GetComponent<Animator>();
        _audioSource = gameObject.GetComponent<AudioSource>();
        _collider2d = gameObject.GetComponent<Collider2D>();
    }

    
    void Update()
    {
        BoundariesCalculation();
        transform.Translate(Vector3.down * _speed * Time.deltaTime);
        
        //this validation is required because the enemy keeps fire after visually dead because the gameobject perse is destroy after 2.8f
        if(!_isDeath)
            FireLaser();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            if (player)
                player.Damage();

            DestroyEnemy();            
        }
        else if (other.tag == "Laser")
        {
            Destroy(other.gameObject);            
            DestroyEnemy();

            if (_player)
                _player.ScorePoints(10);
        }
    }
    #endregion

    #region PrivateMethods
    private void BoundariesCalculation()
    {   
        if (transform.position.y < -5.4f)
        {
            float randX = Random.Range(-9.6f, 9.6f);            
            transform.position = new Vector3(randX, 7,0);
        }
    }
    
    private void DestroyEnemy()
    {
        if (_animator)
            _animator.SetTrigger("OnEnemyDeath");

        _speed = 0;
        _audioSource.Play();
        Destroy(_collider2d);
        _isDeath = true;
        Destroy(this.gameObject, 2.8f);
    }

    private void FireLaser()
    {

        if (Time.time > _canFire)
        {
            _shootRate = Random.Range(3f, 7f);
            _canFire = Time.time + _shootRate;
            GameObject enemyLaser = Instantiate(_laserPrefab, transform.position, Quaternion.identity);
            Laser[] lasers = enemyLaser.GetComponentsInChildren<Laser>();
            foreach(Laser laser in lasers)
            {
                laser.SetEnemyLaser();
            }
        }
    }
    #endregion

}
