﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    #region Properties
    [SerializeField] private GameObject _enemyPrefab;
    [SerializeField] private GameObject _enemyContainer;
    [SerializeField] private GameObject _powerupContainer;    
    [SerializeField] private GameObject[] _powerupPrefabs;
    [SerializeField] private float _enemySpawnRate = 5f;    
    private WaitForSeconds _enemyWaitForSecs;
    private WaitForSeconds _powerupWaitForSecs;
    private WaitForSeconds _startSpawiningWaitForSecs;
    private bool _stopSpawing=false;

    private static SpawnManager _instance;
    public static SpawnManager Instance
    {
        get
        {
            if (_instance == null)
                Debug.LogError("UIManager is null");

            return _instance;
        }
    }

    #endregion

    #region UnityMethods
    private void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        _enemyWaitForSecs = new WaitForSeconds(_enemySpawnRate);
        _powerupWaitForSecs = new WaitForSeconds(Random.Range(3,8));
        _startSpawiningWaitForSecs = new WaitForSeconds(3f);
    }

    #endregion

    #region Coroutines
    IEnumerator SpawnEnemyRoutine()
    {
        yield return _startSpawiningWaitForSecs;

        while (!_stopSpawing)
        {   
            Vector3 spawnPosition = new Vector3(Random.Range(-9.6f, 9.6f), 7, 0);
           var enemy = Instantiate(_enemyPrefab, spawnPosition, Quaternion.identity);
            enemy.transform.SetParent(_enemyContainer.transform);
            yield return _enemyWaitForSecs;
        }
    }

    IEnumerator SpawnPowerupRoutine()
    {
        yield return _startSpawiningWaitForSecs;

        while (!_stopSpawing)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-9.6f, 9.6f), 7, 0);
            var powerup = Instantiate(_powerupPrefabs[Random.Range(0, 6)], spawnPosition, Quaternion.identity);            
            powerup.transform.SetParent(_powerupContainer.transform);
            yield return _powerupWaitForSecs;
        }
    }

    #endregion

    #region PublicMethods

    public void StartSpawning()
    {
        StartCoroutine("SpawnEnemyRoutine");
        StartCoroutine("SpawnPowerupRoutine");
    }

    public void StopSpawning()
    {
        _stopSpawing = true;
    }
    #endregion
}
