﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField] private GameObject _explotionPrefab;
    [SerializeField] private float _rotationSpeed = 10f;

    void Update()
    {
        transform.Rotate(Vector3.forward * _rotationSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Laser")
        {
            Instantiate(_explotionPrefab, transform.position, Quaternion.identity);
            Destroy(other.gameObject);
            SpawnManager.Instance.StartSpawning();
            Destroy(this.gameObject, 0.25f);

        }
    }
}
