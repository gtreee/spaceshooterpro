﻿using System.Collections;
using UnityEngine;

public class Laser : MonoBehaviour
{
    #region Properties
    [SerializeField] private float _speed=10f;
    [SerializeField] private Transform _target;
    [SerializeField] private float _rotateSpeed = 1000f;
    private bool _isEnemyLaser= false;
    private bool _isSeekingTarget = false;    
    private Rigidbody2D _rigidBody;
    #endregion

    #region UnityMethods
    void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        Destroy(this.gameObject, 3f);
       
    }

    void FixedUpdate()
    {
        if (_isEnemyLaser)
            transform.Translate(Vector3.down * _speed * Time.deltaTime);
        else if(_isSeekingTarget)
        {
            SeekingTarget();
        }
        else
        {
            transform.Translate(Vector3.up * _speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && _isEnemyLaser)
        {
            Player player = other.GetComponent<Player>();
            if (player)
                player.Damage();      
        }      
    }
    #endregion

    #region PrivateMethods

    private void SeekingTarget()
    {       
        Vector2 direction = (Vector2)_target.position - _rigidBody.position;
        direction.Normalize();
        float rotateAmount = Vector3.Cross(direction, transform.up).z;
        _rigidBody.angularVelocity = -rotateAmount * _rotateSpeed;
        _rigidBody.velocity = transform.up * _speed;
    }
    #endregion

    #region PublicMethods

    public void SetEnemyLaser()
    {
        _isEnemyLaser = true;
    }

    public void SetSeekingTarget(bool isSeeking)
    {
        _isSeekingTarget = isSeeking;
    }

    #endregion
}
